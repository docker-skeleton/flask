FROM python:3.8-alpine
# build args first
ENV PYTHONUNBUFFERED=1
ARG PROJECT_PORT
ARG PROJECT_NAME
ARG GROUP_NAME
ENV PORT $PROJECT_PORT
ENV PROJECT_NAME $PROJECT_NAME
ENV GROUP_NAME $GROUP_NAME

# Actual dockerfile
EXPOSE $PORT
RUN mkdir -p /srv/$GROUP_NAME/$PROJECT_NAME/logs
WORKDIR /srv/$GROUP_NAME/$PROJECT_NAME
COPY requirements.txt requirements.txt
COPY .flake8 .flake8
# Assume we're mounting the src folder, so do not copy that over.
# Need to copy over the files to allow starting gunicorn.
COPY src/* src/
RUN pip install -r requirements.txt
WORKDIR /srv/$GROUP_NAME/$PROJECT_NAME/src
CMD gunicorn --worker-tmp-dir /dev/shm --workers=2 --threads=4 --worker-class=gthread --log-file=../logs/gunicorn.log --bind 0.0.0.0:$PORT app:app