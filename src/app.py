from flask import Flask
from flask_restful import Api
from os import environ

PORT = environ['PORT']


def create_app():
    return Flask(__name__)


def create_api(app):
    return Api(app)


app = create_app()
api = create_api(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=PORT)
